import React, { useState, useEffect } from 'react';
import { KeyboardAvoidingView,View, StyleSheet, Image, TextInput, Text, TouchableOpacity, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import apiArduino from '../services/apiArduino';

import logo from '../assets/logo_icehot.png';

export default function Login({ navigation }) {

	useEffect(() => {
		// ble.devices;
		
	}, []);


	async function handleLogin() {

		navigation.navigate('Login');
	}

	async function handleApi(m) {
		try {
			const response = await apiArduino.post('/add', { m }); //1: quente 2: frio
			console.log('response > ', response);

			
		} catch (error) {
			console.log('error > ', error);
			alert(error.response.data.error);
		}
	}

	return (
		<KeyboardAvoidingView style={styles.container} behavior="padding" enabled={Platform.OS === 'ios'}>
			<Image style={{width:240, height: 80}} source={logo} />

			<TouchableOpacity onPress={()=>handleApi(1)} style={styles.button}>
				<Text style={styles.buttonText}>Esquentar</Text>
			</TouchableOpacity>
			<TouchableOpacity onPress={()=>handleApi(2)} style={styles.buttonFreeze}>
				<Text style={styles.buttonText}>Esfriar</Text>
			</TouchableOpacity>

			<TouchableOpacity onPress={handleLogin}>
				<View style={styles.signup}>
					<Text style={styles.signupButton}>Login</Text>
				</View>
			</TouchableOpacity>
		</KeyboardAvoidingView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#f5f5f5',
		justifyContent: 'center',
		alignItems: 'center',
		padding: 30
	},

	input: {
		height: 46,
		width: 355,
		alignSelf: 'stretch',
		backgroundColor: '#fff',
		borderWidth: 1,
		borderColor: '#ddd',
		borderRadius: 4,
		paddingHorizontal: 15,
		color: '#000'
	},

	button: {
		height: 46,
		width: 355,
		alignSelf: 'stretch',
		backgroundColor: '#DF4723',
		borderRadius: 4,
		marginTop: 10,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonFreeze: {
		height: 46,
		width: 355,
		alignSelf: 'stretch',
		backgroundColor: '#1a73e8',
		borderRadius: 4,
		marginTop: 10,
		justifyContent: 'center',
		alignItems: 'center'
	},

	buttonText: {
		fontWeight: 'bold',
		color: '#fff',
		fontSize: 16
	},
	signup: {
		flexDirection: 'row',
		alignItems: 'flex-end',
		justifyContent: 'center',
		paddingVertical: 20,
	},
	signupText: {
		color: '#444343',
		fontSize: 16
	},
	signupButton: {
		marginLeft: 3,
		color: '#DF4723',
		fontSize: 16,
		fontWeight: '500'
	},
});
