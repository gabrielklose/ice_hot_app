import React, { useState, useEffect } from 'react';
import { KeyboardAvoidingView,View, StyleSheet, Image, TextInput, Text, TouchableOpacity, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import api from '../services/api';

import logo from '../assets/logo_icehot.png';

export default function Login({ navigation }) {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	useEffect(() => {
		// ble.devices;
		AsyncStorage.getItem('user').then(user => {
			if (user) {
				const resetAction = StackActions.reset({
					index: 0,
					actions: [NavigationActions.navigate({ routeName: 'Main' })],
					key: null
				});
				navigation.dispatch(resetAction);
			}
		});
	}, []);

	async function handleLogin() {
		try {
			const response = await api.post('/login', { email, password });
			console.log('response > ', response);

			const { _id } = response.data;
			if(_id){
				await AsyncStorage.setItem('user', _id);
				// console.log('user > ', _id);
				const resetAction = StackActions.reset({
					index: 0,
					actions: [NavigationActions.navigate({ routeName: 'Main' })],
					key: null
				});
				navigation.dispatch(resetAction);
			}else{
				alert('Senha Incorreta!');
			}
		} catch (error) {
			alert(error.response.data.error);
		}
	}

	async function handleRegister() {

		navigation.navigate('Register');
	}
	async function handleSensores() {

		navigation.navigate('Sensores');
	}

	return (
		<KeyboardAvoidingView style={styles.container} behavior="padding" enabled={Platform.OS === 'ios'}>
			<Image style={{width:240, height: 80}} source={logo} />
			<View styles={{marginTop: 100}}>
				<TextInput
					style={[styles.input, {marginTop: 30}]}
					placeholder="Digite seu E-mail"
					placeholderTextColor="#999"
					autoCapitalize="none"
					autoCorrect={false}
					value={email}
					onChangeText={setEmail}
				/>
				<TextInput
					style={[styles.input, {marginTop: 10}]}
					placeholder="Digite sua Senha"
					placeholderTextColor="#999"
					autoCapitalize="none"
					autoCorrect={false}
					value={password}
					secureTextEntry={true}
					onChangeText={setPassword}
				/>
			</View>

			<TouchableOpacity onPress={handleLogin} style={styles.button}>
				<Text style={styles.buttonText}>Entrar</Text>
			</TouchableOpacity>

			<TouchableOpacity onPress={handleRegister}>
				<View style={styles.signup}>
					<Text style={styles.signupText}>Ainda não possui conta?</Text>
					<Text style={styles.signupButton}>Cadastre-se</Text>
				</View>
			</TouchableOpacity>

			<TouchableOpacity onPress={handleSensores}>
				<View style={styles.signup}>
					<Text style={styles.signupButton}>Testar Sensores</Text>
				</View>
			</TouchableOpacity>
		</KeyboardAvoidingView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#f5f5f5',
		justifyContent: 'center',
		alignItems: 'center',
		padding: 30
	},

	input: {
		height: 46,
		width: 355,
		alignSelf: 'stretch',
		backgroundColor: '#fff',
		borderWidth: 1,
		borderColor: '#ddd',
		borderRadius: 4,
		paddingHorizontal: 15,
		color: '#000'
	},

	button: {
		height: 46,
		width: 355,
		alignSelf: 'stretch',
		backgroundColor: '#DF4723',
		borderRadius: 4,
		marginTop: 10,
		justifyContent: 'center',
		alignItems: 'center'
	},

	buttonText: {
		fontWeight: 'bold',
		color: '#fff',
		fontSize: 16
	},
	signup: {
		flexDirection: 'row',
		alignItems: 'flex-end',
		justifyContent: 'center',
		paddingVertical: 20,
	},
	signupText: {
		color: '#444343',
		fontSize: 16
	},
	signupButton: {
		marginLeft: 3,
		color: '#DF4723',
		fontSize: 16,
		fontWeight: '500'
	},
});
