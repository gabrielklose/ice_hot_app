import React, { useState, useEffect } from 'react';
import { ScrollView,KeyboardAvoidingView,View, StyleSheet, Image, TextInput, Text, TouchableOpacity, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { RNCamera } from 'react-native-camera';
import { StackActions, NavigationActions } from 'react-navigation';
import api from '../services/api';

import logo from '../assets/logo_icehot.png';

export default function Register({ navigation }) {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [nome, setNome] = useState('');
	const [foto, setFoto] = useState(null);

	useEffect(() => {
	}, []);

	async function handleRegister() {
		const response = await api.post('/devs', { nome, email, password, foto });

		const { _id } = response.data;

		await AsyncStorage.setItem('user', _id);

		const resetAction = StackActions.reset({
			index: 0,
			actions: [NavigationActions.navigate({ routeName: 'Main' })],
			key: null
		});
		navigation.dispatch(resetAction);
	}
	const handleBack = () => {
		const resetAction = StackActions.reset({
			index: 0,
			actions: [NavigationActions.navigate({ routeName: 'Login' })],
			key: null
		});
		navigation.dispatch(resetAction);
	};

	const takePicture = async () => {
		try {
			if (this.camera) {
				const options = { quality: 0.5, base64: true };
				const data = await this.camera.takePictureAsync(options);
				console.log('data > ', data);
				setFoto(data.base64);
			}
		} catch (error) {
			console.log('error > ', error);
		}

	};
	return (
		<ScrollView>
			<KeyboardAvoidingView style={styles.container} behavior="padding" enabled={Platform.OS === 'ios'}>
				<Image style={{width:240, height: 80}} source={logo} />
				<View styles={{marginTop: 100}}>
					{!foto && (
						<View>
							<RNCamera
								ref={camera => { this.camera = camera; }}
								style = {{height: 300, borderRadius: 10}}
								type={RNCamera.Constants.Type.front}
								autoFocus={RNCamera.Constants.AutoFocus.on}
								flashMode={RNCamera.Constants.FlashMode.off}
							/>
							<View style={{justifyContent: 'center',alignItems: 'center'}}>
								<TouchableOpacity onPress={takePicture} style={styles.buttonTakePhoto}>
									<Text style={styles.buttonText}>Tirar Foto</Text>
								</TouchableOpacity>
							</View>
						</View>
					)}
					{foto && (
						<View>
							<View style={{justifyContent: 'center',alignItems: 'center'}}>
								<Image style={styles.image} source={{uri: `data:image/jpeg;base64,${foto}`}}/>
								<TouchableOpacity onPress={()=>setFoto(null)} style={styles.buttonTakePhotoAgain}>
									<Text style={styles.buttonText}>Tirar Foto Novamente</Text>
								</TouchableOpacity>
							</View>
						</View>
					)}
					<TextInput
						style={[styles.input, {marginTop: 30}]}
						placeholder="Digite seu Nome"
						placeholderTextColor="#999"
						autoCapitalize="none"
						autoCorrect={false}
						value={nome}
						onChangeText={setNome}
					/>
					<TextInput
						style={[styles.input, {marginTop: 10}]}
						placeholder="Digite seu E-mail"
						placeholderTextColor="#999"
						autoCapitalize="none"
						autoCorrect={false}
						value={email}
						onChangeText={setEmail}
					/>
					<TextInput
						style={[styles.input, {marginTop: 10}]}
						placeholder="Digite sua Senha"
						placeholderTextColor="#999"
						autoCapitalize="none"
						autoCorrect={false}
						value={password}
						secureTextEntry={true}
						onChangeText={setPassword}
					/>
				</View>

				<TouchableOpacity onPress={handleRegister} style={styles.button}>
					<Text style={styles.buttonText}>Cadastrar-se</Text>
				</TouchableOpacity>

				<TouchableOpacity onPress={handleBack}>
					<View style={styles.signup}>
						<Text style={styles.signupText}>Voltar</Text>
					</View>
				</TouchableOpacity>
			</KeyboardAvoidingView>
		</ScrollView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#f5f5f5',
		justifyContent: 'center',
		alignItems: 'center',
		padding: 30
	},

	input: {
		height: 46,
		width: 355,
		alignSelf: 'stretch',
		backgroundColor: '#fff',
		borderWidth: 1,
		borderColor: '#ddd',
		borderRadius: 4,
		paddingHorizontal: 15,
		color: '#000'
	},

	button: {
		height: 46,
		width: 355,
		alignSelf: 'stretch',
		backgroundColor: '#DF4723',
		borderRadius: 4,
		marginTop: 10,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonTakePhoto: {
		height: 46,
		width: 100,
		backgroundColor: '#DF4723',
		borderRadius: 4,
		marginTop: 10,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonTakePhotoAgain: {
		height: 46,
		width: 150,
		backgroundColor: '#DF4723',
		borderRadius: 4,
		marginTop: 10,
		justifyContent: 'center',
		alignItems: 'center'
	},

	buttonText: {
		fontWeight: 'bold',
		color: '#fff',
		fontSize: 16
	},
	signup: {
		flexDirection: 'row',
		alignItems: 'flex-end',
		justifyContent: 'center',
		paddingVertical: 20,
	},
	signupText: {
		color: '#444343',
		fontSize: 16
	},
	signupButton: {
		marginLeft: 3,
		color: '#DF4723',
		fontSize: 16,
		fontWeight: '500'
	},
	image: {
		width: 300,
		height: 300,
		borderRadius: 300 / 2,
		overflow: 'hidden',
		borderWidth: 3,
		borderColor: '#fff',
		resizeMode: 'contain'
	}
});
