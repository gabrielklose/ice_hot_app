import axios from 'axios';

const api = axios.create({
	baseURL: 'http://pi5.webmaveric.net/api'
});

export default api;
