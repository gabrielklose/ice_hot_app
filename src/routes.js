import { createAppContainer, createStackNavigator } from 'react-navigation';

import Login from './pages/Login';
import Main from './pages/Main';
import Register from './pages/Register';
import Sensores from './pages/Sensores';

export default createAppContainer(
	createStackNavigator({
		Login: {
			screen: Login,
			navigationOptions: {
				header: null,
			},
		},
		Main: {
			screen: Main,
			navigationOptions: {
				header: null,
			},
		},
		Register: {
			screen: Register,
			navigationOptions: {
				header: null,
			},
		},
		Sensores: {
			screen: Sensores,
			navigationOptions: {
				header: null,
			},
		},
	})
);
